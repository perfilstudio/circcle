import React from 'react';
import dados from '../dados';
import { Link, withRouter } from 'react-router-dom';

class Editar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: '',
            height: 1080,
            frase: 'Digite sua frase'
        };
        this.tamanhoWindow = this.tamanhoWindow.bind(this);
        this.enviar = this.enviar.bind(this);

        this.achaUser = this.achaUser.bind(this);
        this.achaUser();
    }
    usuario;
    achaUser(){
        dados().usuarios.map(user => {
            if(user.email == localStorage.getItem('usuarioLogado')){
                console.log(user.email);
                console.log(localStorage.getItem('usuarioLogado'));
                this.usuario = user;
                this.state.frase = this.usuario.frase;
            }
        });
    }
    foto;

    enviar(e){
        e.preventDefault();
        this.usuario.frase = this.state.frase;
        this.props.history.push('/home');
    }

    componentDidMount() {
        this.tamanhoWindow();
        window.addEventListener('resize', this.tamanhoWindow);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.tamanhoWindow);
    }

    tamanhoWindow() {
        this.setState({ height: window.innerHeight });
    }

    render() {
        return (
            <div className="total--fixed">
                <form onSubmit={this.handleSubmit}>
                    <div className="container--mini">
                        <div className="dt" style={{height: this.state.height}}>
                            <div className="dt--center">
                                <div className="col--here cursor--pointer">
                                    <div className="col--s4">
                                        <div className="foto-edit relative">
                                            <img src="/images/sem-foto.png" alt=""/>
                                            <div className="edit absolute right bottom"><i className="fas fa-pencil-alt"></i></div>
                                        </div>
                                    </div>
                                    <div className="col--s8">
                                        <div className="dt usuarios-editar width--total">
                                            <div className="dt--center">
                                                <div className="title">Deseja inserir uma foto?</div>
                                                <div className="desc">Arquivos JPG, PNG e GIF de no máximo 2MB.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="relative">
                                    <div className="mt60"></div>
                                    <label htmlFor="status">Status</label>
                                    <div className="mt10"></div>
                                    <input type="text" id="status" className="border--bottom medio no--radius" placeholder="Digite aqui" value={this.state.frase} onChange={(e) => {this.setState({frase: e.target.value})}}/>
                                    <span className="contador-escrita right bottom absolute inline--block">0/20</span>
                                </div>
                                <div onClick={(e) => {this.enviar(e)}} className="absolute botao pequeno botao-roza top right">Salvar</div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default Editar;