import React from 'react'
import dados from '../dados'
import { Link } from 'react-router-dom'
import Links from '../Side/links'
import Menu from '../Side/menu'

class Status extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            home: true
        };
    }
    render() {
        return(
            <div>
                <Links />
                <Menu home={this.state.home} />
                <section className="circcle-conversa">
                    <form onSubmit={this.handleSubmit}>
                        <div className="col--here">
                        <div className="col--s5">
                            <div className="pd--60">
                                <div className="col--s12">
                                    <div className="mt30"></div>
                                    <div className="relative">
                                        <label htmlFor="online_status" className="online--status status">Online</label>
                                        <div className="mt10"></div>
                                        <input type="text" id="online_status" className="border--bottom medio no--radius" placeholder="Digite uma frase para quando estiver online" onChange={this.handleChange}/>
                                        <span className="contador-escrita right bottom absolute inline--block">0/200</span>
                                    </div>
                                    <div className="mt30"></div>
                                    <div className="relative">
                                        <label htmlFor="ausente_status" className="ausente--status status">Ausente</label>
                                        <div className="mt10"></div>
                                        <input type="text" id="ausente_status" className="border--bottom medio no--radius" placeholder="Digite uma frase para quando estiver ausente" onChange={this.handleChange}/>
                                        <span className="contador-escrita right bottom absolute inline--block">0/200</span>
                                    </div>
                                    <div className="mt30"></div>
                                    <div className="relative">
                                        <label htmlFor="offline_status" className="offline--status status">Offline</label>
                                        <div className="mt10"></div>
                                        <input type="text" id="offline_status" className="border--bottom medio no--radius" placeholder="Digite uma frase para quando estiver offline" onChange={this.handleChange}/>
                                        <span className="contador-escrita right bottom absolute inline--block">0/200</span>
                                    </div>
                                    <div className="mt30"></div>
                                    <h2>Configurar hora</h2>
                                    <div className="mt30"></div>
                                </div>
                                <div className="col--s4 configuracoes--offline">
                                    <div className="col--s2 no-padding"><div className="mt15"></div>De</div>
                                    <div className="col--s10 no-padding">
                                        <select id="offline" className="border--bottom medio no--radius" onChange={this.handleChange}>
                                            <option value="00:00">00:00</option>
                                            <option value="00:00">01:00</option>
                                            <option value="00:00">02:00</option>
                                            <option value="00:00">03:00</option>
                                            <option value="00:00">04:00</option>
                                            <option value="00:00">05:00</option>
                                            <option value="00:00">06:00</option>
                                            <option value="00:00">07:00</option>
                                            <option value="00:00">08:00</option>
                                            <option value="00:00">09:00</option>
                                            <option value="00:00">10:00</option>
                                            <option value="00:00">11:00</option>
                                            <option value="00:00">12:00</option>
                                            <option value="00:00">13:00</option>
                                            <option value="00:00">14:00</option>
                                            <option value="00:00">15:00</option>
                                            <option value="00:00">16:00</option>
                                            <option value="00:00">17:00</option>
                                            <option value="00:00">18:00</option>
                                            <option value="00:00">19:00</option>
                                            <option value="00:00">20:00</option>
                                            <option value="00:00">21:00</option>
                                            <option value="00:00">22:00</option>
                                            <option value="00:00">23:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col--s4 configuracoes--offline">
                                    <div className="col--s2 no-padding"><div className="mt15"></div>Até</div>
                                    <div className="col--s10 no-padding">
                                        <select id="offline" className="border--bottom medio no--radius" onChange={this.handleChange}>
                                            <option value="00:00">00:00</option>
                                            <option value="00:00">01:00</option>
                                            <option value="00:00">02:00</option>
                                            <option value="00:00">03:00</option>
                                            <option value="00:00">04:00</option>
                                            <option value="00:00">05:00</option>
                                            <option value="00:00">06:00</option>
                                            <option value="00:00">07:00</option>
                                            <option value="00:00">08:00</option>
                                            <option value="00:00">09:00</option>
                                            <option value="00:00">10:00</option>
                                            <option value="00:00">11:00</option>
                                            <option value="00:00">12:00</option>
                                            <option value="00:00">13:00</option>
                                            <option value="00:00">14:00</option>
                                            <option value="00:00">15:00</option>
                                            <option value="00:00">16:00</option>
                                            <option value="00:00">17:00</option>
                                            <option value="00:00">18:00</option>
                                            <option value="00:00">19:00</option>
                                            <option value="00:00">20:00</option>
                                            <option value="00:00">21:00</option>
                                            <option value="00:00">22:00</option>
                                            <option value="00:00">23:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col--s4 configuracoes--offline">
                                    <select id="offline" className="border--bottom medio no--radius" onChange={this.handleChange}>
                                        <option value="hoje">Somente hoje</option>
                                        <option value="todos_os_dias">Todos os dias</option>
                                        <option value="apenas_final_de_semana">Apenas final de semana</option>
                                    </select>
                                </div>
                                <div className="col--s12 align--right">
                                    <div className="mt30"></div>
                                    <div className="link cursor--pointer">Adicionar hora</div>
                                    <div className="mt30"></div>
                                </div>
                                <div className="col--s12">
                                    <div className="mt30"></div>
                                    <div className="relative">
                                        <label htmlFor="ferias_status" className="ferias--status status">Período de férias</label>
                                        <div className="mt10"></div>
                                        <input type="text" id="ferias_status" className="border--bottom medio no--radius" placeholder="Digite uma frase para quando estiver férias" onChange={this.handleChange}/>
                                        <span className="contador-escrita right bottom absolute inline--block">0/200</span>
                                    </div>
                                    <div className="mt30"></div>
                                    <h2>Configurar hora</h2>
                                    <div className="mt30"></div>
                                </div>
                                <div className="col--s10 configuracoes--offline">
                                    <div className="col--s1 no-padding"><div className="mt15"></div>De</div>
                                    <div className="col--s5 no-padding">
                                        <select id="offline" className="border--bottom medio no--radius" onChange={this.handleChange}>
                                            <option value="00:00">00:00</option>
                                            <option value="00:00">01:00</option>
                                            <option value="00:00">02:00</option>
                                            <option value="00:00">03:00</option>
                                            <option value="00:00">04:00</option>
                                            <option value="00:00">05:00</option>
                                            <option value="00:00">06:00</option>
                                            <option value="00:00">07:00</option>
                                            <option value="00:00">08:00</option>
                                            <option value="00:00">09:00</option>
                                            <option value="00:00">10:00</option>
                                            <option value="00:00">11:00</option>
                                            <option value="00:00">12:00</option>
                                            <option value="00:00">13:00</option>
                                            <option value="00:00">14:00</option>
                                            <option value="00:00">15:00</option>
                                            <option value="00:00">16:00</option>
                                            <option value="00:00">17:00</option>
                                            <option value="00:00">18:00</option>
                                            <option value="00:00">19:00</option>
                                            <option value="00:00">20:00</option>
                                            <option value="00:00">21:00</option>
                                            <option value="00:00">22:00</option>
                                            <option value="00:00">23:00</option>
                                        </select>
                                    </div>
                                    <div className="col--s5 offset--1 no-padding">
                                        <select id="offline" className="border--bottom medio no--radius" onChange={this.handleChange}>
                                            <option value="00:00">00:00</option>
                                            <option value="00:00">01:00</option>
                                            <option value="00:00">02:00</option>
                                            <option value="00:00">03:00</option>
                                            <option value="00:00">04:00</option>
                                            <option value="00:00">05:00</option>
                                            <option value="00:00">06:00</option>
                                            <option value="00:00">07:00</option>
                                            <option value="00:00">08:00</option>
                                            <option value="00:00">09:00</option>
                                            <option value="00:00">10:00</option>
                                            <option value="00:00">11:00</option>
                                            <option value="00:00">12:00</option>
                                            <option value="00:00">13:00</option>
                                            <option value="00:00">14:00</option>
                                            <option value="00:00">15:00</option>
                                            <option value="00:00">16:00</option>
                                            <option value="00:00">17:00</option>
                                            <option value="00:00">18:00</option>
                                            <option value="00:00">19:00</option>
                                            <option value="00:00">20:00</option>
                                            <option value="00:00">21:00</option>
                                            <option value="00:00">22:00</option>
                                            <option value="00:00">23:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col--s12">
                                    <div className="mt30"></div>
                                </div>
                                <div className="col--s10 configuracoes--offline">
                                    <div className="col--s1 no-padding"><div className="mt15"></div>Até</div>
                                    <div className="col--s5 no-padding">
                                        <select id="offline" className="border--bottom medio no--radius" onChange={this.handleChange}>
                                            <option value="00:00">00:00</option>
                                            <option value="00:00">01:00</option>
                                            <option value="00:00">02:00</option>
                                            <option value="00:00">03:00</option>
                                            <option value="00:00">04:00</option>
                                            <option value="00:00">05:00</option>
                                            <option value="00:00">06:00</option>
                                            <option value="00:00">07:00</option>
                                            <option value="00:00">08:00</option>
                                            <option value="00:00">09:00</option>
                                            <option value="00:00">10:00</option>
                                            <option value="00:00">11:00</option>
                                            <option value="00:00">12:00</option>
                                            <option value="00:00">13:00</option>
                                            <option value="00:00">14:00</option>
                                            <option value="00:00">15:00</option>
                                            <option value="00:00">16:00</option>
                                            <option value="00:00">17:00</option>
                                            <option value="00:00">18:00</option>
                                            <option value="00:00">19:00</option>
                                            <option value="00:00">20:00</option>
                                            <option value="00:00">21:00</option>
                                            <option value="00:00">22:00</option>
                                            <option value="00:00">23:00</option>
                                        </select>
                                    </div>
                                    <div className="col--s5 offset--1 no-padding">
                                        <select id="offline" className="border--bottom medio no--radius" onChange={this.handleChange}>
                                            <option value="00:00">00:00</option>
                                            <option value="00:00">01:00</option>
                                            <option value="00:00">02:00</option>
                                            <option value="00:00">03:00</option>
                                            <option value="00:00">04:00</option>
                                            <option value="00:00">05:00</option>
                                            <option value="00:00">06:00</option>
                                            <option value="00:00">07:00</option>
                                            <option value="00:00">08:00</option>
                                            <option value="00:00">09:00</option>
                                            <option value="00:00">10:00</option>
                                            <option value="00:00">11:00</option>
                                            <option value="00:00">12:00</option>
                                            <option value="00:00">13:00</option>
                                            <option value="00:00">14:00</option>
                                            <option value="00:00">15:00</option>
                                            <option value="00:00">16:00</option>
                                            <option value="00:00">17:00</option>
                                            <option value="00:00">18:00</option>
                                            <option value="00:00">19:00</option>
                                            <option value="00:00">20:00</option>
                                            <option value="00:00">21:00</option>
                                            <option value="00:00">22:00</option>
                                            <option value="00:00">23:00</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col--s2 offset--5 align--right">
                            <div className="pd--60">
                                <Link to="/home" className="enviar botao pequeno width--total align--center shadow--pequena">Salvar</Link>
                            </div>
                        </div>
                    </div>
                    </form>
                </section>
            </div>
        );
    }
}
export default Status;