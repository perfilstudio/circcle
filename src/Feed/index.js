import React from 'react'

import Links from '../Side/links';
import Menu from '../Side/menu';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            home: true,
            height: 1080
        };
        this.tamanhoWindow = this.tamanhoWindow.bind(this);

    }

    componentDidMount() {
        this.tamanhoWindow();
        window.addEventListener('resize', this.tamanhoWindow);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.tamanhoWindow);
    }

    tamanhoWindow() {
        this.setState({ height: window.innerHeight });
    }

    render() {
        return (
        <div className="total--fixed">
            <Links />
            <Menu home={this.state.home} />
            <section className="circcle-conversa dt" style={{height: this.state.height}}>
                <div className="dt--center">
                    <div className="container--fluid padding-container">
                        <div className="col--here">
                            <div className="col--s4">
                                <div className="blocos-home width--total dt bloco-rosa align--center cursor--pointer">
                                    <div className="dt--center">
                                        <div className="bloco-qtd">12</div>
                                        <div className="mt5"></div>
                                        <div className="bloco-title">Interações em 3 circcles</div>
                                        <div className="mt5"></div>
                                        <div className="bloco-desc">Fique atento com as notificações!</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col--s4">
                                <div className="blocos-home width--total dt bloco-verde align--center cursor--pointer">
                                    <div className="dt--center">
                                        <div className="bloco-qtd">5</div>
                                        <div className="mt5"></div>
                                        <div className="bloco-title">Demandas para hoje</div>
                                        <div className="mt5"></div>
                                        <div className="bloco-desc">90% foram entregues no prazo</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col--s4">
                                <div className="blocos-home width--total dt bloco-azul align--center cursor--pointer">
                                    <div className="dt--center">
                                        <div className="bloco-qtd">10</div>
                                        <div className="mt5"></div>
                                        <div className="bloco-title">Demandas atrasadas</div>
                                        <div className="mt5"></div>
                                        <div className="bloco-desc">23% foram entregues atrasadas</div>
                                    </div>
                                </div>
                            </div>
                            <div className="item-home col--s4 align--center">
                                <div className="mt60"></div>
                                <img src="images/a1.png" alt=""/>
                                <div className="mt20"></div>
                                <div className="title">Crie um novo circcle</div>
                                <div className="mt15"></div>
                                <div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit Quisque sed risus eu. massa</div>
                            </div>
                            <div className="item-home col--s4 align--center">
                                <div className="mt60"></div>
                                <img src="images/a2.png"  alt=""/>
                                <div className="mt20"></div>
                                <div className="title">Circcles interessantes</div>
                                <div className="mt15"></div>
                                <div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit Quisque sed risus eu. massa</div>
                            </div>
                            <div className="item-home col--s4 align--center">
                                <div className="mt60"></div>
                                <img src="images/a3.png"  alt=""/>
                                <div className="mt20"></div>
                                <div className="title">Acompanhe suas demandas</div>
                                <div className="mt15"></div>
                                <div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit Quisque sed risus eu. massa</div>
                            </div>
                            <div className="item-home col--s4 align--center">
                                <div className="mt60"></div>
                                <img src="images/a4.png"  alt=""/>
                                <div className="mt20"></div>
                                <div className="title">Configure seu status</div>
                                <div className="mt15"></div>
                                <div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit Quisque sed risus eu. massa</div>
                            </div>
                            <div className="item-home col--s4 align--center">
                                <div className="mt60"></div>
                                <img src="images/a5.png" alt="" />
                                <div className="mt20"></div>
                                <div className="title">Todos os arquivos</div>
                                <div className="mt15"></div>
                                <div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit Quisque sed risus eu. massa</div>
                            </div>
                            <div className="item-home col--s4 align--center">
                                <div className="mt60"></div>
                                <img src="images/a6.png" alt="" />
                                <div className="mt20"></div>
                                <div className="title">Pessoas</div>
                                <div className="mt15"></div>
                                <div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit Quisque sed risus eu. massa</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>)
    };

}

export default Home;
