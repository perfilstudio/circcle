import React from 'react';
import { Link } from 'react-router-dom';

class Cadastro extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nome: '',
            sobrenome: '',
            email: '',
            senha: '',
            confirme: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    nome; sobrenome; email; senha; confirme;

    handleChange(event) {
        if(event.target.id === 'nome'){
            this.nome = event.target.value;
        }
        if(event.target.id === 'sobrenome'){
            this.sobrenome = event.target.value;
        }
        if(event.target.id === 'email'){
            this.email = event.target.value;
        }
        if(event.target.id === 'senha'){
            this.senha = event.target.value;
        }
        if(event.target.id === 'confirme'){
            this.confirme = event.target.value;
        }
        this.setState({nome: this.nome, sobrenome: this.sobrenome, email: this.email, senha: this.senha, confirme: this.confirme});
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    render() {
        return (
            <div className="total--fixed">
                <form onSubmit={this.handleSubmit}>
                    <div className="container--mini">
                        <div className="mt60"></div>
                        <h1 className="align--center">Faça seu cadastro</h1>
                        <div className="mt10"></div>
                        <p className="align--center">Continue com</p>
                        <div className="mt30"></div>
                        <div className="col--here">
                            <div className="col--s6">
                                <a role="button" className="facebook botao medio width--total align--center shadow--pequena">Facebook</a>
                            </div>
                            <div className="col--s6">
                                <a role="button" className="google-plus botao medio width--total align--center shadow--pequena">Google Plus</a>
                            </div>
                        </div>
                        <div className="mt30"></div>
                        <p className="align--center">Ou cadastre-se:</p>
                        <div className="mt30"></div>
                        <label htmlFor="nome">Primeiro Nome</label>
                        <div className="mt10"></div>
                        <input type="text" id="nome" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                        <div className="mt30"></div>
                        <label htmlFor="sobrenome">Sobrenome</label>
                        <div className="mt10"></div>
                        <input type="text" id="sobrenome" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                        <div className="mt30"></div>
                        <label htmlFor="email">E-mail</label>
                        <div className="mt10"></div>
                        <input type="text" id="email" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                        <div className="mt30"></div>
                        <label htmlFor="senha">Senha</label>
                        <div className="mt10"></div>
                        <input type="password" id="senha" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                        <div className="mt30"></div>
                        <label htmlFor="confirme">Confirmar Senha</label>
                        <div className="mt10"></div>
                        <input type="password" id="confirme" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                        <div className="mt30"></div>
                        <Link to="/home" className="enviar botao medio width--total align--center shadow--pequena">Continuar</Link>
                        <div className="mt60"></div>
                    </div>
                </form>
            </div>
        );
    }
}

export default Cadastro;