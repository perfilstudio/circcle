export default function () {
    return {
        circcles : [
            {
                _id: 1,
                nome: 'Banco do Brasil',
                participantes: [
                    1,
                    2,
                    3,
                ],
                thumb: '/perfil.png',
            },
            {
                _id: 2,
                nome: 'Banco Santander',
                participantes: [
                    1,
                    2,
                    3,
                ],
                thumb: '/perfil.png',
            },
            {
                _id: 3,
                nome: 'Leite Leco',
                participantes: [
                    1,
                    2,
                ],
                thumb: '/perfil.png',
            },
            {
                _id: 4,
                nome: 'Mitsubishi Motors',
                participantes: [
                    2,
                    3,
                ],
                thumb: '/perfil.png',
            },
            {
                _id: 5,
                nome: 'Draco Studio',
                participantes: [
                    1,
                    2,
                    3
                ],
                thumb: '/perfil.png',
            },
        ],
        usuarios: [
            {
                _id: 1,
                nome: 'Mateus',
                segundo_nome: 'Santana',
                frase: 'Mateus escrita kk',
                email: 'nesantana15@gmail.com',
                senha: 'mateusv8',
            },
            {
                _id: 2,
                nome: 'Thiago',
                segundo_nome: 'Gomes',
                frase: 'Thiago escrita kkk',
                email: 'thiagogomes@gestaoativa.com.br',
                senha: 'teste'
            },
            {
                _id: 3,
                nome: 'William',
                segundo_nome: 'Campos',
                frase: 'William escrita kkk',
                email: 'williamcampos@gestaoativa.com.br',
                senha: 'teste'
            },
        ]
    }
}