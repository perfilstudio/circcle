import React from 'react'
import dados from '../dados'
import { Link, withRouter } from 'react-router-dom'

class Menu extends React.Component {
    constructor(props){
        super(props);
        this.achaUser = this.achaUser.bind(this);
        this.sair = this.sair.bind(this);
        this.achaUser();
    }
    usuario;
    achaUser(){
        dados().usuarios.map(user => {
            if(user.email == localStorage.getItem('usuarioLogado')){
                console.log(user.email);
                console.log(localStorage.getItem('usuarioLogado'));
                this.usuario = user;
            }
        });
    }
    sair(e){
        e.preventDefault();
        localStorage.removeItem('usuarioLogado');
        this.props.history.push('/');
    }

    render() {
        if(!this.props.home){
            return (
            <section className="menu-selecionado">
                <div className="busca">
                    <input type="text" placeholder="Pesquise" />
                </div>
                <div className="dividir-menus"></div>
                <Link to="/novo-circcle" className="novo-circcle" role="button">Novo Circcle</Link>
                <div className="dividir-menus"></div>
                <div className="circcles-relacionados">
                    <Repeat />
                </div>
            </section>
        )}else{
            console.log(this.usuario);
            return(
                <section className="menu-selecionado">
                    <div className="configuracoes">
                        <Link to="/editar-usuario" className="relative width--total cursor--pointer">
                            <span className="edit"></span>
                        </Link>
                        <Link to="/editar" className="configuracoes-item primeiro dt width--total cursor--pointer">
                            <div className="dt--center">
                                <div className="usuario-nome">{this.usuario.nome} {this.usuario.segundo_nome}</div>
                                <div className="usuario-status">{this.usuario.frase}</div>
                            </div>
                        </Link>
                        <div className="configuracoes-item cursor--pointer">
                            <Link to="/status" className="edit"></Link>
                            <Link to="/status" className="config-title">Status</Link>
                            <div className="mt10"></div>
                            <StatusHome />
                        </div>
                        <div className="configuracoes-item">
                            <div className="config-title">Configurações</div>
                            <Link to="/uso-de-dados" className="config-desc cursor--pointer">Uso de dados</Link>
                            <div className="config-desc cursor--pointer">Notificações</div>
                        </div>
                        <div className="configuracoes-item">
                            <div className="config-title">Temas</div>
                            <div className="config-desc cursor--pointer">Black Sky</div>
                            <div className="config-desc cursor--pointer">White Tiger</div>
                            <div className="config-desc cursor--pointer">Bebop</div>
                            <div className="config-desc cursor--pointer">Cinderela</div>
                        </div>
                        <div className="configuracoes-item">
                            <Link to="/como-usar" className="config-desc" style={{'paddingTop' : 0}}>Como usar o Circcle</Link>
                            <Link to="/encerrar-conta" className="config-desc cursor--pointer">Encerrar conta</Link>
                            <div onClick={(event) => {this.sair(event)}} className="config-desc cursor--pointer">Fazer logout</div>
                        </div>
                    </div>
                </section>
            );
        }
    };

}

export default Menu;

class StatusHome extends React.Component {
    render() {
        return(
            <div>
                <div className="input--radio">
                    <input type="radio" name="status" id="online"/>
                    <label htmlFor="online">Online</label>
                </div>
                <div className="mt15"></div>
                <div className="input--radio">
                    <input type="radio" name="status" id="ausente"/>
                    <label htmlFor="ausente">Ausente</label>
                </div>
                <div className="mt15"></div>
                <div className="input--radio">
                    <input type="radio" name="status" id="offline"/>
                    <label htmlFor="offline">Offline</label>
                </div>
                <div className="mt15"></div>
                <div className="input--radio">
                    <input type="radio" name="status" id="ferias"/>
                    <label htmlFor="ferias">Período de Férias</label>
                </div>
            </div>
        );
    }
}

const circcles = dados();

const linksCirccles = circcles.circcles.map((x, index) => {
    return (
    <div key={index}>
        <Link to={'/circcle/'+x._id} className="circcles-item">
            <div className="circcle-title">{x.nome}</div>
            <div className="circcle-desc">Ficou muito bom!! Manda uma có ...</div>
            <div className="circcle-notif"></div>
        </Link>
        <div className="dividir-menus"></div>
    </div>
    )
});

class Repeat extends React.Component {
    render() {
        return(
            <div>
                {linksCirccles}
            </div>
        );
    }
}