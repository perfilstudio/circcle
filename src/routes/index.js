import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Login from '../Login';
import Home from '../Feed';
import Cadastro from '../Cadastro/index';
import Editar from '../Cadastro/editar';
import Circcle from '../Circcle';
import Status from '../Status';
import ComoUsar from '../Pages/como-usar';
import UsuDados from "../Pages/uso-de-dados";
import Encerrar from "../Pages/encerrar";
import Usuario from "../Cadastro/usuario";
import NovoCirccle from "../Circcle/criar";

export default () => (
    <BrowserRouter>
        <div>
            <Route path="/" exact component={Login} />
            <Route path="/home" exact component={Home} />
            <Route path="/cadastro" exact component={Cadastro} />
            <Route path="/editar" exact component={Editar} />
            <Route path="/status" exact component={Status} />
            <Route path="/editar-usuario" exact component={Usuario} />
            <Route path="/como-usar" exact component={ComoUsar} />
            <Route path="/uso-de-dados" exact component={UsuDados} />
            <Route path="/encerrar-conta" exact component={Encerrar} />
            <Route path="/circcle" exact component={Circcle} />
            <Route path="/circcle/:id" exact component={Circcle} />
            <Route path="/novo-circcle" exact component={NovoCirccle} />
        </div>
    </BrowserRouter>
);