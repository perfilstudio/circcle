import React from 'react'
import dados from '../dados'
import { Link } from 'react-router-dom'
import Links from '../Side/links'
import Menu from '../Side/menu'

class Usuario extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            home: true
        };
    }
    render() {
        return(
            <div>
                <Links />
                <Menu home={this.state.home} />
                <section className="circcle-conversa">
                    <form onSubmit={this.handleSubmit}>
                        <div className="col--here">
                            <div className="col--s5">
                                <div className="pd--60">
                                    <div className="col--s4">
                                        <div className="foto-edit relative">
                                            <img src="/images/sem-foto.png" alt=""/>
                                            <div className="edit absolute right bottom"><i class="fas fa-pencil-alt"></i></div>
                                        </div>
                                    </div>
                                    <div className="col--s12">
                                        <div className="mt30"></div>
                                        <label htmlFor="nome">Primeiro Nome</label>
                                        <div className="mt10"></div>
                                        <input type="text" id="nome" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                                        <div className="mt30"></div>
                                        <label htmlFor="sobrenome">Sobrenome</label>
                                        <div className="mt10"></div>
                                        <input type="text" id="sobrenome" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                                        <div className="mt30"></div>
                                        <label htmlFor="email">E-mail</label>
                                        <div className="mt10"></div>
                                        <input type="text" id="email" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                                        <div className="mt30"></div>
                                        <label htmlFor="senha">Senha</label>
                                        <div className="mt10"></div>
                                        <input type="password" id="senha" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                                        <div className="mt30"></div>
                                        <label htmlFor="confirme">Confirmar Senha</label>
                                        <div className="mt10"></div>
                                        <input type="password" id="confirme" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                                        <div className="mt30"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col--s2 offset--5 align--right">
                                <div className="pd--60">
                                    <Link to="/home" className="enviar botao pequeno width--total align--center shadow--pequena">Salvar</Link>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        );
    }
}
export default Usuario;