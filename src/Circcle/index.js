import React from 'react'

import Links from '../Side/links';
import Menu from '../Side/menu';

import dados from '../dados'

class Circcle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            home: false
        };
    }
    render() {
        if(this.props.match.params.id){
            const circcles = dados();
            const circcleSelcionado = circcles.circcles.filter(item => {
                if(item._id == this.props.match.params.id){
                    return item;
                }
            });
            let usuarios = [];
            const usuariosCirccle = circcleSelcionado[0].participantes.map((participante) => {
                circcles.usuarios.map((usuario, x) => {
                    if(participante == usuario._id){
                        usuarios.push(<div key={x}>
                                <div className="participantes-dentro cursor--pointer">
                                    <div className="participantes-fotos"><div className="perfil"><span></span></div></div>
                                    <div className="participantes-desc">
                                        <div className="participantes-nome">{usuario.nome}</div>
                                        <div className="participantes-status">{usuario.status}</div>
                                    </div>
                                    <div className="hover--pessoas-aparece">
                                        <div className="col--here pd--15">
                                            <div className="col--s12">
                                                <div className="link--adc-pessoas cursor--pointer">Ver perfil</div>
                                            </div>
                                        </div>
                                        <div className="separador"></div>
                                        <div className="col--here pd--15">
                                            <div className="col--s12">
                                                <div className="link--adc-pessoas cursor--pointer">Enviar mensagem privada</div>
                                            </div>
                                        </div>
                                        <div className="separador"></div>
                                        <div className="col--here pd--15">
                                            <div className="col--s12">
                                                <div className="link--adc-pessoas cursor--pointer">Tornar administrador</div>
                                            </div>
                                        </div>
                                        <div className="separador"></div>
                                        <div className="col--here pd--15">
                                            <div className="col--s12">
                                                <div className="link--adc-pessoas cursor--pointer">Remover do circcle</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="dividir-menus"></div>
                            </div>);
                    }
                });
            });
            console.log(usuarios);
            return (
                <div className="total--fixed">
                    <Links />
                    <Menu home={this.state.home} />
                    <section className="circcle-conversa">
                        <div className="circcle-participantes">
                            {usuarios}
                        </div>
                        <div className="circcle-area">
                            <div className="area-topo">
                                <div className="col--here">
                                    <div className="col--s1" style={{'paddingRight' : 0}}>
                                        <div className="image-circcle"></div>
                                    </div>
                                    <div className="col--s5" style={{'marginTop' : '14px'}}>
                                        <div className="nome-circcle">{circcleSelcionado[0].nome}</div>
                                        <div className="desc-circcle">30 pessoas | 250 demandas | 103 arquivos | 5gb de 10 </div>
                                    </div>
                                    <div className="col--s6">
                                        <ul className="comandos-circcle">
                                            <li><a href="" className="ativo">Conversa</a></li>
                                            <li><a href="">Arquivos</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="area-conversa">
                                <div className="separador"><span>Segunda, 28 de Nov 2017</span></div>
                                <div className="dividir-menus"></div>
                                <div className="item-conversa">
                                    <div className="item-conversa-user-nome">Mateus Santana<span>16:12</span></div>
                                    <div className="item-conversa-user-mensagem">Nunc arcu lacus, euismod eget feugiat a, lobortis vel felis qua maximus enim</div>
                                </div>
                            </div>
                            <div className="area-digitacao">
                                <div className="arquivo-relativo">
                                    <input type="text" className="browser-default" placeholder="Digite aqui sua mensagem, @pesquise ou mande @privado" />
                                    <i className="material-icons anexar-arquivo">attach_file</i>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>)
        }else{
            return (
                <div>
                    <Links />
                    <Menu home={this.state.home} />
                    <section className="circcle-conversa">
                        <div>Selecione um circulo</div>
                    </section>
                </div>
            )
        }
    };

}

export default Circcle;