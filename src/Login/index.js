import React from 'react';
import dados from '../dados';
    import { Link, withRouter } from 'react-router-dom';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            senha: ''
        };

        this.entrar = this.entrar.bind(this);

    }

    entrar(e) {
        e.preventDefault();
        dados().usuarios.map(user => {
           if(user.email == this.state.email){
               if(user.senha == this.state.senha){
                   localStorage.setItem('usuarioLogado', user.email);
                   this.props.history.push('/home');
               }else{
                   console.log('Senha invalida');
               }
           }else{
               console.log('User não encontrado');
           }
        });
    }

    render() {
        dados().usuarios.map(user => {
            if (user.email == localStorage.getItem('usuarioLogado')) {
                this.props.history.push('/home');
            }
        });
        return (
            <div className="total--fixed">
                <Link to="/cadastro" className="absolute top right link-topo">Ainda não tenho cadastro</Link>
                <form>
                    <div className="container--mini">
                        <div className="mt60"></div>
                        <h1 className="align--center">Faça seu login</h1>
                        <div className="mt10"></div>
                        <p className="align--center">Continue com</p>
                        <div className="mt20"></div>
                        <div className="col--here">
                            <div className="col--s6">
                                <a role="button" className="facebook botao medio width--total align--center shadow--pequena">Facebook</a>
                            </div>
                            <div className="col--s6">
                                <a role="button" className="google-plus botao medio width--total align--center shadow--pequena">Google Plus</a>
                            </div>
                        </div>
                        <div className="mt60"></div>
                        <label htmlFor="email">E-mail</label>
                        <div className="mt10"></div>
                        <input type="text" id="email" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={(event) => {this.setState({email: event.target.value})}}/>
                        <div className="mt30"></div>
                        <label htmlFor="senha">Senha</label>
                        <div className="mt10"></div>
                        <input type="senha" id="senha" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={(event) => {this.setState({senha: event.target.value})}}/>
                        <div className="mt30"></div>
                        <button type="submit" onClick={(event) => {this.entrar(event)}} className="enviar botao medio width--total align--center shadow--pequena">Entrar</button>
                        <div className="mt30"></div>
                        <a role="button" className="link align--center">Esqueci minha senha</a>
                    </div>
                </form>
            </div>
        );
    }
}

export default Login;