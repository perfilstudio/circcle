import React from 'react';
import { Link } from 'react-router-dom';

class NovoCirccle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: '',
            height: 1080
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.tamanhoWindow = this.tamanhoWindow.bind(this);
    }

    foto;

    handleChange(event) {
        this.setState({status: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    componentDidMount() {
        this.tamanhoWindow();
        window.addEventListener('resize', this.tamanhoWindow);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.tamanhoWindow);
    }

    tamanhoWindow() {
        this.setState({ height: window.innerHeight });
    }

    render() {
        return (
            <div className="total--fixed">
                <form onSubmit={this.handleSubmit}>
                    <div className="container--mini">
                        <div className="dt" style={{height: this.state.height}}>
                            <div className="dt--center">
                                <div className="col--here cursor--pointer">
                                    <div className="col--s4">
                                        <div className="foto-edit relative">
                                            <img src="/images/sem-foto.png" alt=""/>
                                            <div className="edit absolute right bottom"><i class="fas fa-pencil-alt"></i></div>
                                        </div>
                                    </div>
                                    <div className="col--s8">
                                        <div className="dt usuarios-editar width--total">
                                            <div className="dt--center">
                                                <div className="title">Insira uma imagem</div>
                                                <div className="desc">Arquivos JPG, PNG e GIF de no máximo 2MB.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="relative">
                                    <div className="mt60"></div>
                                    <label htmlFor="nome_circcle">Nome do circcle</label>
                                    <div className="mt10"></div>
                                    <input type="text" id="nome_circcle" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                                    <span className="contador-escrita right bottom absolute inline--block">0/200</span>
                                </div>
                                <div className="relative">
                                    <div className="mt30"></div>
                                    <label htmlFor="sub_frase">Subfrase</label>
                                    <div className="mt10"></div>
                                    <input type="text" id="sub_frase" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                                    <span className="contador-escrita right bottom absolute inline--block">0/200</span>
                                </div>
                                <div className="relative add--pessoas">
                                    <div className="mt30"></div>
                                    <h2>Adicione pessoas</h2>
                                    <div className="mt30"></div>
                                    <div className="hover--pessoas">
                                        <img src="images/add.png" alt=""/>
                                    </div>
                                    <div className="hover--pessoas">
                                        <img src="images/perfil.png" alt=""/>
                                        <div className="hover--pessoas-aparece">
                                            <div className="col--here pd--15">
                                                <div className="col--s4">
                                                    <img src="images/perfil.png" alt=""/>
                                                </div>
                                                <div className="col--s8 no-padding">
                                                    <div className="mt5"></div>
                                                    <div className="nome">Roberto Carlos</div>
                                                    <div className="cargo">Gerente de Marketing</div>
                                                </div>
                                            </div>
                                            <div className="separador"></div>
                                            <div className="col--here pd--15">
                                                <div className="col--s12">
                                                    <div className="link--adc-pessoas cursor--pointer">Tornar administrador</div>
                                                </div>
                                            </div>
                                            <div className="separador"></div>
                                            <div className="col--here pd--15">
                                                <div className="col--s12">
                                                    <div className="link--adc-pessoas cursor--pointer">Remover do circcle</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Link to="/home" className="absolute botao pequeno botao-roza top right">Criar</Link>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default NovoCirccle;