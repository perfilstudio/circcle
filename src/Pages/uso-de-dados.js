import React from 'react'
import dados from '../dados'
import { Link } from 'react-router-dom'
import Links from '../Side/links'
import Menu from '../Side/menu'

class UsuDados extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            home: true
        };
    }
    render() {
        return(
            <div>
                <Links />
                <Menu home={this.state.home} />
                <section className="circcle-conversa">
                    <div className="col--here">
                        <div className="col--s6">
                            <div className="pd--60">
                                <div className="col--s12">
                                    <h1>Uso de Dados</h1>
                                    <div className="mt10"></div>
                                    <p>Você tem disponível 1GB de espaço para armazenamento.</p>
                                    <div className="mt30"></div>
                                    <img src="images/uso-dados.png" alt=""/>
                                </div>
                            </div>
                        </div>
                        <div className="col--s3 offset--3 align--right">
                            <div className="pd--60">
                                <Link to="/home" className="seja-premium botao pequeno width--total align--center shadow--pequena">
                                    <img src="images/star.png" alt=""/>
                                    Seja premium
                                </Link>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
export default UsuDados;