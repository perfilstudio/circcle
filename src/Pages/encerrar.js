import React from 'react'
import dados from '../dados'
import { Link } from 'react-router-dom'
import Links from '../Side/links'
import Menu from '../Side/menu'

class Encerrar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            home: true
        };
    }
    render() {
        return(
            <div>
                <Links />
                <Menu home={this.state.home} />
                <section className="circcle-conversa">
                    <form onSubmit={this.handleSubmit}>
                        <div className="col--here">
                        <div className="col--s6">
                            <div className="pd--60">
                                <div className="col--s12">
                                    <h1>Encerrar conta</h1>
                                    <div className="mt10"></div>
                                    <p>Siga os passos abaixo para encerrar sua conta.</p>
                                    <div className="mt30"></div>
                                    <div className="texto-encerrar">
                                        <p>
                                            <strong>Termos importantes</strong><br/><br/>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris efficitur orci sed dolor faucibus, ac scelerisque lectus gravida. Nulla tempor libero ante, eget porta sem pulvinar eu. Aliquam erat volutpat. Sed vulputate in quam eu hendrerit. Sed quis arcu tempor, viverra nisi at, egestas lacus.<br/><br/>
                                            Phasellus id mauris et diam rutrum aliquet ut dictum neque. Nulla faucibus velit at ipsum dapibus, eu aliquam nisi elementum. Cras euismod tellus lectus, id faucibus ipsum commodo ut. Praesent sodales, urna ut gravida euismod, ante massa egestas erat, et auctor tortor neque a mauris. Nulla at hendrerit urna. Vestibulum condimentum magna quis leo efficitur, vel aliquet quam fermentum. In maximus dui vel nunc aliquam sodales. Integer varius odio eu egestas commodo. Pellentesque feugiat eros eget lorem viverra, eget imperdiet neque facilisis.<br/><br/>
                                            Phasellus blandit eros velit, in accumsan felis vestibulum id. Praesent sed libero blandit, fringilla est ut, tempus quam. Aliquam elit mi, congue euismod rhoncus ut, porta ut elit. Etiam eget pretium purus, a luctus lacus. Donec venenatis convallis gravida. Nullam dolor mi, semper at lacinia eu, aliquet ac lectus. Morbi felis sem, lobortis sed justo nec, ornare dapibus ex. Phasellus sit amet sollicitudin orci, scelerisque hendrerit lacus.<br/><br/>
                                            Sed vitae nisi ullamcorper, malesuada magna sit amet, porttitor neque. Sed placerat est vel ligula sollicitudin congue ac dignissim augue. Praesent tempus tellus nibh, et vestibulum lorem rhoncus id. Quisque nec sapien pellentesque, pulvinar tortor varius, tempus odio. Proin in nisi a libero dictum mollis. Vivamus nec quam augue. Integer consectetur mattis consectetur. Phasellus vel tellus in libero eleifend volutpat.
                                        </p>
                                    </div>
                                    <div className="mt30"></div>
                                    <div className="input--checkbox">
                                        <input type="checkbox" id="aceitar" value="aceitar"/>
                                        <label htmlFor="aceitar">Aceito os termos</label>
                                    </div>
                                    <div className="mt60"></div>
                                            <label htmlFor="senha">Digite a senha</label>
                                            <div className="mt10"></div>
                                            <input type="text" id="senha" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                                            <div className="mt30"></div>
                                            <label htmlFor="confirma-senha">Confirmar senha</label>
                                            <div className="mt10"></div>
                                            <input type="password" id="confirma-senha" className="border--bottom medio no--radius" placeholder="Digite aqui" onChange={this.handleChange}/>
                                </div>
                            </div>
                        </div>
                        <div className="col--s2 offset--4 align--right">
                            <div className="pd--60">
                                <Link to="/" className="enviar botao pequeno width--total align--center shadow--pequena">Encerrar</Link>
                            </div>
                        </div>
                    </div>
                    </form>
                </section>
            </div>
        );
    }
}
export default Encerrar;