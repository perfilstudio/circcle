const express = require('express');
const app = require('express');
const http = require('http').Server(app);
const io = require('socket.io')(http);

io.on('connection', (socket) => {
    console.log('Nós temos uma conecção');
    socket.on('nova-mensagem', (msg) => {
        console.log(msg);
        io.emit("Teste", msg);
    });
});

http.listen('3000', () => {
    console.log('Nós estamos conectados');
});